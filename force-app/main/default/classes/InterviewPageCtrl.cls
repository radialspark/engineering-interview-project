public with sharing class InterviewPageCtrl {
	//Page Properties
	public List<Account> acctList {get;set;}
	public Id acctId {get;set;}
	public List<Contact> conList {get;set;}

	public InterviewPageCtrl() {
		acctList = new List<Account>();
		conList = new List<Contact>();
		queryAccounts();
	}

	public void queryAccounts() {
		acctList = [SELECT Name, Id FROM Account];
		return acctList;
	}

	public void queryContacts() {
		//Query for Contacts related to the Account
		return conList;
	}
}
